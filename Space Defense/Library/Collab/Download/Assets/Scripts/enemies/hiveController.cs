﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hiveController : MonoBehaviour {

    public float positionFloor = 100;
    public float positionCieling = 600;

    private Vector3 position;

    private List<GameObject> enemies;
    private GameObject leader;
    private GameObject target;

    private float radius;
    private float radiusGrowthPerMember = 0.3f;
    private float radiusBaseSize = 10;

    private float updateRate = 1;
    private float timer = 0;


    enum modes { wander, pickup, attack };

    modes mode;

    public hiveController(GameObject l)
    {
        leader = l;
        position = leader.transform.position;
        
    }

	// Use this for initialization
	void Start () {
        enemies = new List<GameObject>();
        radius = 0;
	}
	
	// Update is called once per frame
    //TODO finish this.
	void Update () {
        if (isDead())
        {
            return;
        }

        ///DO hivey things in here.
        ///
        switch (mode)
        {
            case modes.attack:
                attackMode();

                break;

            case modes.pickup:
                pickupMode();
                break;

            case modes.wander:

                wanderMode();
                break;
        }


	}

    public void addEnemy(GameObject e)
    {
        enemies.Add(e);
        updateRadius();
    }

    public void removeEnemy(GameObject e)
    {
        if(leader == e)
        {
            leader = null;
        }
        else
        {
            enemies.Remove(e);
        }

        updateRadius();
    }

    public bool isDead()
    {
        return leader == null;
    }

    //merges the other hive into this one. kills the other leader if it is still alive.
    public void merge(hiveController other)
    {
        //removes the other leader from existence.
        if(other.leader!= null)
        {
            other.leader.GetComponent<enemyHealth>().health = 0;
        }
        other.leader = null;

        //adds the other list of enemies to this hive.
        this.enemies.AddRange(other.enemies);
        other.enemies.Clear();

        updateRadius();
    }

    private void updateRadius()
    {
        radius = radiusBaseSize + (enemies.Count * radiusGrowthPerMember);
    }


    private void attackMode()
    {
        
        
    }

    private void pickupMode()
    {
        
    }

    private void wanderMode()
    {
        timer -= Time.deltaTime;

        if(timer < 0)
        {
            //update the timer;
            timer = updateRate;

            //do stuff;

            foreach(GameObject curEnemy in enemies)
            {
                curEnemy.GetComponent<enemyController>().setModeWander(position, radius);
            }

            
            //radius divided by 10 because leaders should stay close to the middle.
            leader.GetComponent<enemyController>().setModeWander(position, radius / 10);
                
           
            
        }
    }

    public void setModeAttack()
    {
        mode = modes.attack;

        foreach (GameObject curEnemy in enemies)
        {
            curEnemy.GetComponent<enemyController>().setModeChase();
        }


        leader.GetComponent<enemyController>().setModeChase();
    }

    public void setModePickup(GameObject target)
    {
        this.target = target;
        mode = modes.pickup;

        foreach (GameObject curEnemy in enemies)
        {
            curEnemy.GetComponent<enemyController>().setModeWander(leader.transform.position, radius);
        }

        leader.GetComponent<enemyController>().setModePickup(target);
    }

    public void setModeWander()
    {
        timer = 0;
        wanderMode();
    }
}
