﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class motherShipSpin : MonoBehaviour {

    public float rotSpd = 1;
    public float nullArea = 0.5f;

    private GameObject focus;

	// Use this for initialization
	void Start () {
        focus = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 dif = focus.transform.position - this.transform.position;
        float amountToTheRight = Vector3.Dot(dif, this.transform.right);

        if (amountToTheRight > nullArea)
        {
            this.transform.Rotate(0, rotSpd * Time.deltaTime, 0);
        }
        else if(amountToTheRight < nullArea)
        {
            this.transform.Rotate(0, -rotSpd * Time.deltaTime, 0);
        }
	}
}
