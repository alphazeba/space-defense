﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapitalFly : MonoBehaviour {

    private Vector3 start = new Vector3(0, 0, -10000);
    private Vector3 end = new Vector3(0, 0, -1000);

    public int volleySize = 100;
    public GameObject missile;

    public Transform[] ports;

	// Use this for initialization
	void Start () {
        transform.position = start;

        StartCoroutine(fire());
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, end, 0.01f);
    }

    IEnumerator fire()
    {
        while (true)
        {
            enemyController[] enemies = FindObjectsOfType<enemyController>();
            for (int x = 0; x < volleySize; x++)
            {
                foreach (Transform t in ports)
                {
                    GameObject g = Instantiate(missile, t.position, t.rotation);
                    //g.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity;
                    if (enemies.Length > 0)
                    {
                        g.GetComponent<MissileScript>().SetTarget(enemies[x % enemies.Length].transform);
                    }
                }
            }
            yield return new WaitForSeconds(1);
        }
    }
}
