﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelectController : MonoBehaviour
{

    public Button asteroidButton;
    public Button multiButton;
    public Button alienButton;
    public Button lavaButton;
    public Button planetButton;
    public Button backButton;

    // Use this for initialization
    void Start()
    {
        asteroidButton.onClick.AddListener(onAsteroidButton);
        multiButton.onClick.AddListener(onMultiButton);
        alienButton.onClick.AddListener(onAlienButton);
        lavaButton.onClick.AddListener(onLavaButton);
        planetButton.onClick.AddListener(onPlanetButton);
        backButton.onClick.AddListener(onBackButton);
    }

    void onAsteroidButton()
    {
        asteroidButton.GetComponentInChildren<Text>().text = "Loading";
        SceneManager.LoadScene("Level_Small 1");
    }

    void onMultiButton()
    {
        asteroidButton.GetComponentInChildren<Text>().text = "Loading";
        SceneManager.LoadScene("Level_Multi 1");
    }

    void onAlienButton()
    {
        asteroidButton.GetComponentInChildren<Text>().text = "Loading";
        SceneManager.LoadScene("Level_Alien 1");
    }

    void onLavaButton()
    {
        asteroidButton.GetComponentInChildren<Text>().text = "Loading";
        SceneManager.LoadScene("Level_Lava 1");
    }

    void onPlanetButton()
    {
        asteroidButton.GetComponentInChildren<Text>().text = "Loading";
        SceneManager.LoadScene("Level_Large");
    }

    void onBackButton()
    {
        asteroidButton.GetComponentInChildren<Text>().text = "Loading";
        SceneManager.LoadScene("menuScene");
    }
}
