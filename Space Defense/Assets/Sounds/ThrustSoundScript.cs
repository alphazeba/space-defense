﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrustSoundScript : MonoBehaviour {

    public AudioSource source;

	void FixedUpdate () {
        source.volume = Input.GetAxis("Vertical");
	}
}
