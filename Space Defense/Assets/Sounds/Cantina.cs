﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cantina : MonoBehaviour {

    private AudioSource a;

	// Use this for initialization
	void Start () {
        a = GetComponent<AudioSource>();
        a.mute = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider c)
    {
        a.mute = false;
    }

    void OnTriggerExit(Collider c)
    {
        a.mute = true;
    }
}
