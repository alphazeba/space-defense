﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mineLayerController : MonoBehaviour {

    public GameObject mine;
    public float minePeriod = 10;
    public float timer = 0;

    private GameObject player;
    private enemyOverlordController overlord;

	// Use this for initialization
	void Start () {
        timer = minePeriod;

        enemyController ec = this.GetComponent<enemyController>();
        overlord = ec.getOverlord();
        player = ec.getPlayer();
	}
	
	// Update is called once per frame
	void Update () {

        timer -= Time.deltaTime;

		if(timer< 0)
        {
            timer = minePeriod;

            GameObject newMine = Instantiate(mine, this.transform.position + (this.transform.forward * -9), this.transform.rotation);
            newMine.GetComponent<mineController>().Initialize(player);
        }
	}
}
