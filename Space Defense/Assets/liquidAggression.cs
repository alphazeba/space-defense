﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class liquidAggression : MonoBehaviour {

    private enemyController ec;

	// Use this for initialization
	void Start () {
        ec = this.GetComponent<enemyController>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!ec.isAttacking())
        {
            ec.setModeChase();
        }

	}
}
