﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereGravity : MonoBehaviour {

	private Rigidbody rb;
	private centralBody[] planets;
	private float acceleration = 2.0f;

    private bool isGrabbed = false;

    public bool IsGrabbed
    {
        get
        {
            return isGrabbed;
        }

        set
        {
            isGrabbed = value;
        }
    }

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        planets = FindObjectsOfType<centralBody>();
	}
	
	// Update is called once per frame
	void Update () {
        centralBody planet = planets[0];

        for (int i = 1; i < planets.Length; i++)
        {
            if (Vector3.Distance(transform.position, planets[i].transform.position) < Vector3.Distance(transform.position, planet.transform.position))
            {
                planet = planets[i];
            }
        }

		var direction = (planet.getPos() - transform.position).normalized;

        if (!isGrabbed)
        {
            rb.AddForceAtPosition(
                Physics.gravity.magnitude * direction * acceleration, // pull towards planet center
                transform.position - transform.up, // pull from feet to orient character
                ForceMode.Acceleration // gravity is pure acceleration
            );

            this.transform.Translate(Vector3.forward * 10 * Time.deltaTime);
        }


	}
}
