﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class motherShipMovement : MonoBehaviour {

    public float orbitSpd = 10;

    private GameObject centralBody;

	// Use this for initialization
	void Start () {
        centralBody = GameObject.FindGameObjectWithTag("CentralBody");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        this.transform.RotateAround(centralBody.transform.position, Vector3.left, orbitSpd * Time.deltaTime);
	}
}
