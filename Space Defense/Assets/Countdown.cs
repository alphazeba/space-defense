﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour {

    private Text t;
    public float time = 180.0f;
    public GameObject thingToSpawn;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;

        t.text = minutes + ":" + seconds;
	}

    private void FixedUpdate()
    {
        time -= Time.deltaTime;
        if (time < 0.0f)
        {
            t.enabled = false;
            Instantiate(thingToSpawn);
            Destroy(this);
        }
    }
}
