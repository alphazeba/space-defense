﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class motherShipModes : MonoBehaviour {

    public GameObject bigExplosion;

    public float attackTime  = 5;
    public float suckTime = 10;
    public float damagedTime = 10;

    enum modes { attack, suck, damaged };
    modes mode;

    public GameObject heart;

    public GameObject forcefieldAnchor;
    public GameObject tractorBeamAnchor;
    public GameObject[] generatorAnchor;


    public GameObject forcefieldObject;
    public GameObject tractorBeamEffect;

    public GameObject generatorObject;

    public GameObject bullet;

    private GameObject beam;
    private List<GameObject> generators;

    private List<GameObject> caughtPeople;

    private GameObject forcefield;

    private float timer;

    private GameObject player;
    public GameObject bulletPrefab;

    private int lasthealth;

    public GameObject heartHitEffect;
    
    

	// Use this for initialization
	void Start () {
        generators = new List<GameObject>();
        caughtPeople = new List<GameObject>();
        player = GameObject.FindGameObjectWithTag("Player");

        lasthealth = heart.GetComponent<enemyHealth>().health;
        //DEBUG STUFF FROM HERE DOWN>


        startAttack();

	}
	
	// Update is called once per frame
	void Update () {
        //timer
        timer -= Time.deltaTime;


        //health

        if(heart == null)
        {
            //we have died
            die();
            return;
        }
        if (lasthealth > heart.GetComponent<enemyHealth>().health) 
        {
            //make an explosion at heart.
            Instantiate(heartHitEffect, heart.transform.position, heart.transform.rotation);
            print("last health = " +lasthealth + "heart health = " + heart.GetComponent<enemyHealth>().health);
        }
        lasthealth = heart.GetComponent<enemyHealth>().health;


        //keep forcefield in place.
        if (forcefield != null)
        {
            forcefield.transform.rotation = forcefieldAnchor.transform.rotation;
            forcefield.transform.position = forcefieldAnchor.transform.position;
        }

        switch (mode)
        {
            case modes.attack:

                //shoot at the player.

                shoot(10, 0.1f);
                shoot(1, 10);
                shoot(10, 3);

                if(timer < 0)
                {
                    startSuck();
                }

                break;

            case modes.suck:

                if(timer < 0)
                {
                    startAttack();
                }

                //set tractorbeam effect and generators (if they still exist in the correct location)
                beam.transform.position = tractorBeamAnchor.transform.position;
                beam.transform.rotation = tractorBeamAnchor.transform.rotation;


                bool allDead = true; //used to check if there are any generators that are still alive.
                for(int i = 0; i < generatorAnchor.Length; ++i)
                {
                    if(generators[i] != null)
                    {
                        generators[i].transform.rotation = generatorAnchor[i].transform.rotation;
                        generators[i].transform.position = generatorAnchor[i].transform.position;
                        allDead = false;//there is at least one generator remaining.
                    }
                }

                //if all the generatos are dead start damaged mode.
                if (allDead)
                {
                    //TODO stop tractor beam
                    startDamaged();
                }


                //if not, apply force to the caught people and drag them towards the ship.
                //TODO apply force to the people list.

                break;

            case modes.damaged:

                if(timer < 0)
                {
                    startAttack();
                }

                //wait around for a bit.

                //maybe shoot slowly and incredibly inaccurately.

                //maybe shoot particles out from heart area.
                break;
        }

	}


    private void startAttack()
    {

        timer = attackTime;
        mode = modes.attack;

        //TODO make sure that the shield is up.
        if(forcefield == null)
        {

            forcefield = Instantiate(forcefieldObject, forcefieldAnchor.transform.position, forcefieldAnchor.transform.rotation);
        }
        //destroy generators and tractor beam if necessary;

        foreach(GameObject g in generators)
        {
            if (g!= null)
            {
                g.GetComponent<enemyHealth>().damage(10);
            }
        }
        generators.Clear();

        Destroy(beam);
    }

    private void startSuck()
    {
        timer = suckTime;
        mode = modes.suck;

        //TODO fill a list or array with player on the motherships side of the planet.
        //STILL NEED TO DO THIS>


        

        //build the generators
        for(int i = 0; i < generatorAnchor.Length; ++i)
        {
            generators.Add(Instantiate(generatorObject, generatorAnchor[i].transform.position, generatorAnchor[i].transform.rotation));
        }


        //build the tractor beam.
        beam = Instantiate(tractorBeamEffect, tractorBeamAnchor.transform.position, tractorBeamAnchor.transform.rotation);

    }

    private void startDamaged()
    {
        timer = damagedTime;
        //remove the shield. to reveal the mothership's heart.
        //also remove the tractor beam

        forcefield.GetComponent<enemyHealth>().damage(100000);
        Destroy(beam);
    }


    private void shoot(float shootRandom, float inaccuracy)
    {
        //fire at player
        if (Random.Range(0.0f, shootRandom) < 1.0f && (this.transform.position - player.transform.position).sqrMagnitude < 1600000)
        {
            Quaternion inaccuracyQ = Quaternion.Euler(Random.Range(-inaccuracy, inaccuracy), Random.Range(-inaccuracy, inaccuracy), Random.Range(-inaccuracy, inaccuracy));

            Vector3 interceptPoint = FirstOrderIntercept(transform.position, Vector3.zero,500, player.transform.position, player.GetComponent<Rigidbody>().velocity);
            Debug.DrawLine(transform.position, interceptPoint);

            Vector3 direction = (interceptPoint - transform.position).normalized;
            Quaternion firingDirection = Quaternion.LookRotation(direction);
           // if (Quaternion.Angle(Quaternion.LookRotation(transform.forward), firingDirection) < 45.0f)
            {
                GameObject g = Instantiate(bulletPrefab, transform.position, firingDirection * inaccuracyQ);
                //g.GetComponent<Rigidbody>().velocity += rb.velocity;
            }
        }
    }

    //first-order intercept using absolute target position
    public static Vector3 FirstOrderIntercept
    (
        Vector3 shooterPosition,
        Vector3 shooterVelocity,
        float shotSpeed,
        Vector3 targetPosition,
        Vector3 targetVelocity
    )
    {
        Vector3 targetRelativePosition = targetPosition - shooterPosition;
        Vector3 targetRelativeVelocity = targetVelocity - shooterVelocity;
        float t = FirstOrderInterceptTime
        (
            shotSpeed,
            targetRelativePosition,
            targetRelativeVelocity
        );
        return targetPosition + t * (targetRelativeVelocity);
    }
    //first-order intercept using relative target position
    public static float FirstOrderInterceptTime
    (
        float shotSpeed,
        Vector3 targetRelativePosition,
        Vector3 targetRelativeVelocity
    )
    {
        float velocitySquared = targetRelativeVelocity.sqrMagnitude;
        if (velocitySquared < 0.001f)
            return 0f;

        float a = velocitySquared - shotSpeed * shotSpeed;

        //handle similar velocities
        if (Mathf.Abs(a) < 0.001f)
        {
            float t = -targetRelativePosition.sqrMagnitude /
            (
                2f * Vector3.Dot
                (
                    targetRelativeVelocity,
                    targetRelativePosition
                )
            );
            return Mathf.Max(t, 0f); //don't shoot back in time
        }

        float b = 2f * Vector3.Dot(targetRelativeVelocity, targetRelativePosition);
        float c = targetRelativePosition.sqrMagnitude;
        float determinant = b * b - 4f * a * c;

        if (determinant > 0f)
        { //determinant > 0; two intercept paths (most common)
            float t1 = (-b + Mathf.Sqrt(determinant)) / (2f * a),
                    t2 = (-b - Mathf.Sqrt(determinant)) / (2f * a);
            if (t1 > 0f)
            {
                if (t2 > 0f)
                    return Mathf.Min(t1, t2); //both are positive
                else
                    return t1; //only t1 is positive
            }
            else
                return Mathf.Max(t2, 0f); //don't shoot back in time
        }
        else if (determinant < 0f) //determinant < 0; no intercept path
            return 0f;
        else //determinant = 0; one intercept path, pretty much never happens
            return Mathf.Max(-b / (2f * a), 0f); //don't shoot back in time
    }

    private void die()
    {
        //dying animations and other stuff
        //give a bunch of points for winning 

        for(int i = 0; i < 1000; ++i)
        {
            BroadcastMessage(GameEvent.ENEMY_DIED);
        }


        for(int i = 0; i < 40; ++i)
        {
            Vector3 rand = Random.insideUnitSphere * 100;
            Instantiate(bigExplosion, this.transform.position + rand, this.transform.rotation);
        }

        Destroy(this.gameObject);
    }
}
