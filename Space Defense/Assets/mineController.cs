﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mineController : MonoBehaviour {

    public GameObject target;
    public float spd;
    public float triggerRadius = 400;
    public float explodeRadius = 100;

    private bool triggered = false;

	// Use this for initialization
	void Start () {
		
	}

    public void Initialize(GameObject t)
    {
        target = t;
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        Vector3 dif = target.transform.position - this.transform.position;
        if (triggered)
        {

            Vector3 normalAtTarget = dif;
            normalAtTarget.Normalize();

            this.transform.position += normalAtTarget * (spd * Time.deltaTime);

            if(dif.magnitude < explodeRadius)
            {
                target.GetComponent<PlayerHealth>().Damage(10 + Random.Range(0, 5));
                this.gameObject.GetComponent<enemyHealth>().damage(2);
                
            }
        }
        else
        {
            if(dif.magnitude < triggerRadius)
            {
                triggered = true;
                this.GetComponent<AudioSource>().Play();
            }
        }
		
	}
}
