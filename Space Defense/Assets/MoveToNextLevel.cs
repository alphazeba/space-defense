﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveToNextLevel : MonoBehaviour {

    public float time;
    public int next;
    public GameObject warpEffect;
    public Transform ship;

    private bool done = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeSinceLevelLoad > time)
        {
            SceneManager.LoadScene(next);
        }
        if (Time.timeSinceLevelLoad > time - 4)
        {
            if (!done)
            {
                done = true;
                Instantiate(warpEffect, ship);
            }
        }
	}
}
