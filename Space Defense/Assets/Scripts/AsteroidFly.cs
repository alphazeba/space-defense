﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidFly : MonoBehaviour {

	private GameObject target;
	private Vector3 direction;
	private float speed;
	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Player");
		direction = target.transform.position - this.transform.position;
		speed = Random.Range (0.05f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Translate (direction * Time.deltaTime * speed);
		if (this.transform.position.z < -15000.0f) {
			Destroy (this.gameObject);
		}
		if(Vector3.Distance(target.transform.position, this.transform.position) < 100.0f){
			target.gameObject.GetComponent<PlayerHealth>().Damage(5);
		}
	}
		
}
