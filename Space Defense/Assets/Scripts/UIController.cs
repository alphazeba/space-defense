﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    public Button storyModeButton;
    public Button endlessModeButton;
    public Button quitButton;

	// Use this for initialization
	void Start () {
        storyModeButton.onClick.AddListener(onStoryModeButton);
        endlessModeButton.onClick.AddListener(onEndlessModeButton);
        quitButton.onClick.AddListener(onQuitButton);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void onStoryModeButton()
    {
        //PlayerPrefs.SetString("PlayerMode","Single");
        //Debug.Log("Set PlayerMode Single");
        storyModeButton.GetComponentInChildren<Text>().text = "Loading";
        SceneManager.LoadScene("introScene");
    }

    void onEndlessModeButton()
    {
        endlessModeButton.GetComponentInChildren<Text>().text = "Loading";
        SceneManager.LoadScene("levelSelect");
    }

    void onQuitButton()
    {
        Application.Quit();
    }
}
