﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyController : MonoBehaviour {

    public float turnSpd = 20;  //turnign rate of the enmey.
    public float acceleration = 50; //acceleration of the enemy.
    public float speed;
    public float targetRadius = 12; //distance from the target the enemy needs to be before the enemy begins moving to the next target.

    public float inaccuracy = 5.0f;

    public float shootRandom = 60.0f;

    public GameObject bulletPrefab;

    public GameObject personKilledExplosion;

    public string type;

    private enemyOverlordController overlord;

    private int hiveTag;

    public hiveController inHive;

    public float captureHeightMultiplier = 5; //how many times larger than the planet to fly up.

    #region ai mode enum

    enum modes { test, chase , wander ,pickup,carry};


    #endregion

    #region private variables

    #region targetting stuff
    private class TargetNode
    {
        public TargetNode()
        {
            pos = new Vector3();
            spd = 0;
        }

        public TargetNode(Vector3 v , float s)
        {
            pos = v;
            spd = s;
        }

        public TargetNode(TargetNode t)
        {
            pos = t.pos;
            spd = t.spd;
        }

        public Vector3 pos; //the goal position
        public float spd;  //the goal speed to have. //will be used to have ais alter their speed.
    };

    private Queue<TargetNode> targetNodes;

    private TargetNode curTarget;
    private TargetNode lastTarget;

    private TargetNode dodgePlanet;

    private bool planetDodgeHasBeenChecked;

    #endregion

    private modes mode;

    private GameObject player;
    private GameObject targetPerson;

    private Rigidbody self;

    private Vector3 lastPos;

    private Vector3 commandPos; //the command position is used by the overlord to specify an area along with a command.
    private float commandFloat; //additional info along with the command.

    #endregion

    // Use this for initialization
    void Start () {

        self = this.GetComponent<Rigidbody>();
        targetNodes = new Queue<TargetNode>();
        lastTarget = new TargetNode(this.transform.position, 0);
        curTarget = null;
        dodgePlanet = null;
        planetDodgeHasBeenChecked = false;

        lastPos = this.transform.position;

        //just for testing purposes i set mode to test.
        mode = modes.test;


        inHive = null;

        overlord.addEnemy(this.gameObject);

	}

    public void Initialize(enemyOverlordController overlord)
    {
        this.overlord = overlord;
        player = overlord.getPlayer();
    }

    void OnDestroy()
    {
        if(mode == modes.carry)
        {
            //we must release the target person if killed while holding a person
            targetPerson.GetComponent<SphereGravity>().IsGrabbed = false;
        }

        overlord.removeEnemy(this.gameObject);
        if(inHive != null)
        {

            inHive.removeEnemy(this.gameObject);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (mode != modes.carry && other.collider.CompareTag("Person"))
        {
            if (!other.collider.gameObject.GetComponent<SphereGravity>().IsGrabbed)
            {
                targetPerson = other.collider.gameObject;
                setModeCarry();
            }
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        updateAI();

        updateTargets();

        if(curTarget != null) //if there is a curTarget
        {
            follow(curTarget);
        }
	}

    #region private ai functions.

    private void updateAI()
    {
        switch (mode)
        {
            case modes.test:
                test();
                break;
            case modes.chase:
                chase();
                break;
            case modes.wander:
                wander();
                break;
            case modes.pickup:
                pickup();
                break;
            case modes.carry:
                carry();
                break;
        }
    }

    private void test()
    {
        if(curTarget == null)
        {
            pushTarget(player.transform.position, speed);
            pushTarget(this.transform.position, speed);
        }
    }

    private void chase()
    {
        setImmediateTarget(player.transform.position, speed);

        //fire at player
        if (Random.Range(0.0f, shootRandom) < 1.0f && (this.transform.position - player.transform.position).sqrMagnitude < 160000)
        {
            Quaternion inaccuracyQ = Quaternion.Euler(Random.Range(-inaccuracy, inaccuracy), Random.Range(-inaccuracy, inaccuracy), Random.Range(-inaccuracy, inaccuracy));

            Vector3 interceptPoint = FirstOrderIntercept(transform.position, Vector3.zero, 200.0f, player.transform.position, player.GetComponent<Rigidbody>().velocity);
            Debug.DrawLine(transform.position, interceptPoint);

            Vector3 direction = (interceptPoint - transform.position).normalized;
            Quaternion firingDirection = Quaternion.LookRotation(direction);
            if (Quaternion.Angle(Quaternion.LookRotation(transform.forward), firingDirection) < 45.0f)
            {
                GameObject g = Instantiate(bulletPrefab, transform.position, firingDirection * inaccuracyQ);
                //g.GetComponent<Rigidbody>().velocity += rb.velocity;
            }
        }
    }

    private void wander()
    {
        //command float is the wandering range radius.
        //command pos is the area around which to wander.

        if(curTarget == null)
        {
            if(Random.Range(0,5) < 1.2f)
            {
                doAFlip();//does a flip for fun.
            }
            else
            {
                pushTarget(commandPos + (Random.insideUnitSphere * commandFloat), speed * Random.Range(0.5f, 1)); //wanders to another location in the wander point.
            }
        }
    }

    private void pickup()
    {

        if(curTarget == null)
        {
            setModeCarry();
        }
        else
        {
            setImmediateTarget(targetPerson.transform.position, speed*0.9f); //doesn't go full speed while doing this. makes it a bit easier for the player.
            if (targetPerson.GetComponent<SphereGravity>().IsGrabbed)
            {
                mode = modes.test;
                setModeWander();
            }
        }
    }

    private void carry()
    {
        //keep the target stuck to this enemy.
        Vector3 holdLocation = this.transform.position + this.transform.forward * -6;
        targetPerson.GetComponent<Rigidbody>().position = holdLocation;

        if(curTarget == null)
        {
            succesfullyStoleAPerson();
        }
    }


    private void succesfullyStoleAPerson()
    {
        //TODO
        //fill this in with actual stuff.
        //kill the targetperson.
        //kill this object and replace it with a mutated enemy.
        Instantiate(personKilledExplosion, this.transform.position,this.transform.rotation);
        Destroy(targetPerson);
        targetPerson = null;
        mode = modes.test;
        setModeWander();
    }

    #endregion

    #region public ai mode setters

    public void setModeWander(Vector3 location , float r )
    {
        if(mode != modes.pickup && mode!= modes.carry)
        {
            mode = modes.wander;
            setImmediateTarget(commandPos, speed);
            commandPos = location;
            commandFloat = r;
        }
    }

    public void setModeWander()
    {

        if (mode != modes.pickup && mode != modes.carry)
        {
            if (mode != modes.wander)
            {
                setImmediateTarget(lastTarget.pos, speed);
                curTarget = null;
            }
            mode = modes.wander;
        }
    }

    public void setModeChase()
    {
        if (mode != modes.pickup && mode != modes.carry)
        {
            mode = modes.chase;
        }
    }

    public void setModePickup(GameObject targetPerson)
    {
        this.targetPerson = targetPerson;
        if(mode!= modes.pickup && mode != modes.carry)
        {
            mode = modes.pickup;
            setImmediateTarget(targetPerson.transform.position, speed * 0.9f);
        }
    }

    public void setModeCarry()
    {
        //TODO 
        //fill this in with stuff.
        mode = modes.carry;

        //setup the target to be in hold mode.
        targetPerson.GetComponent<SphereGravity>().IsGrabbed = true;

        //set destination.
        //probbaly like 500 straight up.
        Vector3 direction = this.transform.position;
        //direction.Normalize();
        //changed this so it's triple the radius of the planet approx - W

        //set target.
        setImmediateTarget(direction * captureHeightMultiplier, speed * 0.75f);
    }

    #endregion

    #region funtions to mess with the target list.

    private void updateTargets()
    {
        //check if you have arrived at the target
        //if near enough to a target, set it to null.
        if(curTarget != null)
        {
            if ((curTarget.pos - this.transform.position).magnitude < targetRadius)
            {
                lastTarget = curTarget;
                curTarget = null;
            }
        }

        //if the current target is null, try and get another target from the target Queue
        if(curTarget == null && targetNodes.Count != 0)  //there is not a current target and tagetnodes is not empty.
        {
            curTarget = targetNodes.Dequeue();
            planetDodgeHasBeenChecked = false;
        } 
    }

    private void pushTarget(Vector3 pos, float spd)
    {
        //pushes a target onto the list of targets
        targetNodes.Enqueue(new TargetNode(pos, spd));
    }

    private void setImmediateTarget(Vector3 pos, float spd)
    {
        //immediately sets the target as the current goal
        if(targetNodes != null)
        {
            targetNodes.Clear();
        }
        lastTarget = curTarget;
        planetDodgeHasBeenChecked = false;
        curTarget = new TargetNode(pos, spd);
    }

    #endregion

    #region movement related stuff.

    private void follow(TargetNode target)
    {
        Vector3 velocityVector = this.transform.position - lastPos;
        float velocityMagnitude = velocityVector.magnitude / Time.deltaTime;

        if(velocityMagnitude > target.spd)
        {
            decelerate();
        }
        else if(velocityMagnitude < target.spd)
        {
            accelerate();
        }

        turn(target.pos);

        //update the ship to continue facing forward.
        if (velocityVector.magnitude != 0.0f)
        {
            this.transform.rotation = Quaternion.LookRotation(velocityVector);
        }


        //update last position
        lastPos = this.transform.position;
    }

    private void doAFlip()
    {
        Quaternion rot = Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
        pushTarget(this.transform.position + rot * new Vector3(0, 0, 12), speed);
        pushTarget(this.transform.position, speed);
    }

    
    

    private void accelerate()
    {
        self.AddForce(this.transform.forward * acceleration);
    }

    private void decelerate()
    {
        self.AddForce(this.transform.forward * -acceleration);
    }

    private void turn(Vector3 targetPos)
    {
        Vector3 targetDiff = targetPos - this.transform.position;
        Vector3 tangent = targetDiff - (this.transform.forward * Vector3.Dot(this.transform.forward,targetDiff));
        tangent.Normalize();

        //apply the force.
        self.AddForce((tangent * turnSpd));
    }

    #endregion

    #region getters and setters

    public float getSqrDistFromPlayer()
    {
        Vector3 dif = player.transform.position - this.transform.position;

        return dif.sqrMagnitude;
    }

    public void moveHive(Vector3 location, float r)
    {
        commandPos = location;
        commandFloat = r;
    }

    public bool isPickingUp()
    {
        return mode == modes.pickup;
    }

    public bool isCarrying()
    {
        return mode == modes.carry;
    }

    public bool isAttacking()
    {
        return mode == modes.chase;
    }

    #endregion



    //first-order intercept using absolute target position
    public static Vector3 FirstOrderIntercept
    (
        Vector3 shooterPosition,
        Vector3 shooterVelocity,
        float shotSpeed,
        Vector3 targetPosition,
        Vector3 targetVelocity
    )
    {
        Vector3 targetRelativePosition = targetPosition - shooterPosition;
        Vector3 targetRelativeVelocity = targetVelocity - shooterVelocity;
        float t = FirstOrderInterceptTime
        (
            shotSpeed,
            targetRelativePosition,
            targetRelativeVelocity
        );
        return targetPosition + t * (targetRelativeVelocity);
    }
    //first-order intercept using relative target position
    public static float FirstOrderInterceptTime
    (
        float shotSpeed,
        Vector3 targetRelativePosition,
        Vector3 targetRelativeVelocity
    )
    {
        float velocitySquared = targetRelativeVelocity.sqrMagnitude;
        if (velocitySquared < 0.001f)
            return 0f;

        float a = velocitySquared - shotSpeed * shotSpeed;

        //handle similar velocities
        if (Mathf.Abs(a) < 0.001f)
        {
            float t = -targetRelativePosition.sqrMagnitude /
            (
                2f * Vector3.Dot
                (
                    targetRelativeVelocity,
                    targetRelativePosition
                )
            );
            return Mathf.Max(t, 0f); //don't shoot back in time
        }

        float b = 2f * Vector3.Dot(targetRelativeVelocity, targetRelativePosition);
        float c = targetRelativePosition.sqrMagnitude;
        float determinant = b * b - 4f * a * c;

        if (determinant > 0f)
        { //determinant > 0; two intercept paths (most common)
            float t1 = (-b + Mathf.Sqrt(determinant)) / (2f * a),
                    t2 = (-b - Mathf.Sqrt(determinant)) / (2f * a);
            if (t1 > 0f)
            {
                if (t2 > 0f)
                    return Mathf.Min(t1, t2); //both are positive
                else
                    return t1; //only t1 is positive
            }
            else
                return Mathf.Max(t2, 0f); //don't shoot back in time
        }
        else if (determinant < 0f) //determinant < 0; no intercept path
            return 0f;
        else //determinant = 0; one intercept path, pretty much never happens
            return Mathf.Max(-b / (2f * a), 0f); //don't shoot back in time
    }

    public int getHiveTag()
    {
        return hiveTag;
    }

    public enemyOverlordController getOverlord()
    {
        return overlord;
    }

    public GameObject getPlayer()
    {
        return player;
    }
}
