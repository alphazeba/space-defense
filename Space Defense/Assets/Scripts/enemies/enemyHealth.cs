﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyHealth : MonoBehaviour {

    public int health = 4;
    public GameObject explode;

    public void damage(int dmg)
    {
        health -= dmg;
        Messenger.Broadcast(GameEvent.ENEMY_HIT);
       

        if (health <= 0)
        {
            Messenger.Broadcast(GameEvent.ENEMY_DIED);
         
            Instantiate(explode, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject);
        }
    }

    public void heal(int hp)
    {
        health += hp;
    }
}
