﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//i need to repush this.

public class waveSpawnerController : MonoBehaviour {

    public GameObject Spawner;
    public GameObject Enemy;
    public int numberToSpawn = 20;
    public float waveSpawnTime = 5;
    public int preferredGroupSize = 5;
    private float radius;

    private int numOfSpawners;
    private int excess;
    private float spawnPeriod;

    private enemyOverlordController overlord;
    

    private void SpawnSpawner(int n)
    {
        Quaternion rot = Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
        Vector3 pos = rot * new Vector3(0, 0, -radius); //uses negative rotation so that rot is facing towards the origin.


        GameObject spawner = Instantiate(Spawner, pos, rot);

        spawner.GetComponent<enemySpawnerController>().Initialize(this.Enemy, n, this.spawnPeriod , overlord);

        spawner = null;
        
    }
    
    public void Initialize(GameObject go , int nToSpwna , int groupSize , float r , enemyOverlordController overlord)
    {
        this.Enemy = go;
        this.numberToSpawn = nToSpwna;
        this.preferredGroupSize = groupSize;
        this.radius = r;

        this.overlord = overlord;

        numOfSpawners = numberToSpawn / preferredGroupSize;
        excess = numberToSpawn % preferredGroupSize;

        spawnPeriod = waveSpawnTime / preferredGroupSize;

        for (int i = 0; i < numOfSpawners; ++i)
        {
            SpawnSpawner(preferredGroupSize);
        }

        if (excess > 0)
        {
            SpawnSpawner(excess);
        }

        StartCoroutine(destroyInAMoment());
    }

    IEnumerator destroyInAMoment()
    {
        yield return new WaitForSeconds(5.0f);
        Destroy(gameObject);
    }
}
