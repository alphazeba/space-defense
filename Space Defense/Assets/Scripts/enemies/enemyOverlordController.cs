﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyOverlordController : MonoBehaviour {

    private class Hive
    {
        public int population;
        public Vector3 position;
        public float radius;
        public bool wasMoved;

        public Hive(Vector3 position, float radius)
        {
            this.position = position;
            this.radius = radius;
            population = -1;
            wasMoved = false;
        }

        public void moveHive(Vector3 position, float radius)
        {
            this.position = position;
            this.radius = radius;
            wasMoved = true;
        }
    }

    public GameObject LanderEnemy;
    public GameObject LeaderEnemy;
    public GameObject MineEnemy;
    public GameObject WaveSpawner;
    public GameObject Player;

    public float updatePeriod = 5;
    public int initialWaveSize = 20;
    public float waveGrowth = 1.3f;
    public float spawningRadius;
    public float chaseRadius = 20;

    public int TimeUntilForceNextWave = 120;

    private int backupTimerLength = 8;
    private int backupTimer = 100;



    private bool continueLooping;
    private int updatesSinceLastSpawn;
    private int waveSize;
    private int lastHives;
    private Hive[] hive;
    private GameObject[] person;
    private List<GameObject> enemies;
    private List<hiveController> hives;

    public int EnemyCount()
    {
        return enemies.Count;
    }

    void Awake()
    {
        Messenger.AddListener(GameEvent.END_OF_WAVE, onEndOfWave);
    }

    void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.END_OF_WAVE, onEndOfWave);
    }

    // Use this for initialization
    void Start() {
        //initialize.
        continueLooping = true;
        updatesSinceLastSpawn = 10000;//something high so the first wave is spawned right away. //edited to give a bit of a break before start - W
        waveSize = initialWaveSize;
        hive = null;
        person = null;
        lastHives = 1;
        backupTimer = -1;

        enemies = new List<GameObject>();
        hives = new List<hiveController>();

        InfrequentUpdate();
    }

    void Update()
    {
        if (hives.Count > 0)
        {
            foreach (hiveController hive in hives)
            {
                hive.manualUpdate();
            }
        }
    }

    //TODO add inthe hive controller updare.
    //called periodically. affected by updateperiod.
    void InfrequentUpdate()
    {
        
        if(Player == null)
        {
            Player = GameObject.FindGameObjectWithTag("Player");
        }

       //update the spawn counter.
        ++updatesSinceLastSpawn;


        //commands will now be given to hives, not enemies directly.
        if(hives.Count > 0)
        {
            //update the backup timer.
            backupTimer = backupTimerLength;

            //if there are loose enemies, they should be thrown into a hive.
            foreach(GameObject enemy in enemies)
            {
                if(enemy.GetComponent<enemyController>().inHive == null)
                {
                    //put this enemy into a hive.
                    hives[Random.Range(0, hives.Count)].addEnemy(enemy);
                    break;
                }
            }

            //hives should already have been defined.
            //update the hives.
                //inside hive update, have hive movement and checking for player distance in order to attack.
                /*
            foreach(hiveController hive in hives)
            {
                hive.manualUpdate();
            }
            */

            //command some hives to go on pickup duty.


            //if there are no enemies attempting to steal people then force one to steal somebody.
            int minNumPickingup = enemies.Count / 6;
            if (minNumPickingup == 0)
            {
                minNumPickingup = 1;
            }

            int numPickingup = 0;
            foreach(hiveController hive in hives)
            {
                if (hive.isPickingUp())
                {
                    ++numPickingup;
                }
            }

            //command enough enemies to pickup.
            if (numPickingup < minNumPickingup)
            {
                //force a pickup.
                for (int i = 0; i < minNumPickingup - numPickingup; ++i)
                {
                    orderPickup(hives[Random.Range(0, hives.Count)]);
                }
            }

            //clear the person array
            person = null;
        }
        else
        {
            --backupTimer;
            if (backupTimer < 0)
            {
                backupTimer = backupTimerLength;
                print("line 168 braodcast ran");
                Messenger.Broadcast(GameEvent.END_OF_WAVE);
            }
        }  
        

        //declare the end of the wave.
        if( (lastHives >0 && hives.Count == 0 ) || (updatePeriod * updatesSinceLastSpawn > TimeUntilForceNextWave))
        {
            print("line 175 broadcast ran");
            Messenger.Broadcast(GameEvent.END_OF_WAVE);
        }

        print("hives count = " + hives.Count);

        //finishing things
        lastHives = hives.Count;


        //delay call to continue loop.
        if (continueLooping)
        {
            StartCoroutine(waitThenUpdate(updatePeriod));
        }
        else
        {
            //we destroy this.
            Destroy(gameObject);
        }
    }

    //doesn't need hive update.
    IEnumerator waitThenUpdate(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        InfrequentUpdate();
    }

    //shouldn't need to be updated for hive.
    private void spawnLanderWave(int n)
    {
        GameObject wave = Instantiate(WaveSpawner, this.transform.position, this.transform.rotation);
        wave.GetComponent<waveSpawnerController>().Initialize(LanderEnemy, n * 31/40, 5, spawningRadius,this);
        wave = Instantiate(WaveSpawner,this.transform.position,this.transform.rotation);
        wave.GetComponent<waveSpawnerController>().Initialize(LeaderEnemy, n / 5, 2, spawningRadius, this);
        wave = Instantiate(WaveSpawner, this.transform.position, this.transform.rotation);
        wave.GetComponent<waveSpawnerController>().Initialize(MineEnemy, n / 40, 1, spawningRadius, this);
    }

    //this function has been hivified.
    private void orderPickup(hiveController minion)
    { 
        
        if(person == null)
        {
            person = GameObject.FindGameObjectsWithTag("Person");
        }

        //find closest person to this minion.
        float minDistance = 100000;
        GameObject closestTarget = null;
        for(int i=  0; i< person.Length; ++i)
        {
            float distance = minion.getSquareLeaderDistance(person[i].transform.position);
            if(distance < minDistance)
            {
                minDistance = distance;
                closestTarget = person[i];
            }
        }

        //set minion to pickup that target person.
        if(closestTarget != null)
        {
            minion.setModePickup(closestTarget);
        }
    }

    //shouldn't need to updated for hive.
    private void onEndOfWave()
    {
        spawnLanderWave(waveSize);
        waveSize = Mathf.CeilToInt(waveSize * waveGrowth);
        updatesSinceLastSpawn = 0;
    }

    //shouldn't need to be updated for hive.
    public GameObject getPlayer()
    {
        return Player;
    }

    //this function has been hivified.
    public void addEnemy(GameObject e)
    {
        //maintain the master enemy list.
        enemies.Add(e);

        if (e.GetComponent<enemyController>().type == "leader")
        {
            print("a new hive has been created.");
            //make new hive.
            hives.Add(new hiveController(e));
        }
        else
        {
            //add the enemy to smallest existing hive.
            //unless the hive currently being looked at has less than 5 in it.
            //in that case immediately put the enemy in that hive.

            int indexOfSmallestHive = -1;
            int smallestNum = 10000;
            for(int i = 0; i < hives.Count; ++i)
            {
                int curSize = hives[i].getSize();
                if (curSize < 5)
                {
                    indexOfSmallestHive = i;
                    break;
                }
                else if (curSize < smallestNum)
                {
                    smallestNum = curSize;
                    indexOfSmallestHive = i;
                }
            }

            if(indexOfSmallestHive > -1)
            {
                hives[indexOfSmallestHive].addEnemy(e);
            }
        }
    }

    //this function has been hivified.
    public void removeEnemy(GameObject e)
    {

        if(e.GetComponent<enemyController>().type == "leader")
        {
            //find the hive that this leader belongs to.
            hiveController leaderHive = null;
            foreach(hiveController hive in hives)
            {
                if (hive.compareLeader(e))
                {
                    leaderHive = hive;
                }
            }

            //seperate this hive from the main hive list.
            hives.Remove(leaderHive);

            //if there are other hives,
            //merge this hive's minions into a differetn hive.
        
            if(hives.Count > 0)
            {
                hives[Random.Range(0,hives.Count)].merge(leaderHive);
                leaderHive = null;
            }
            //else destroy the last hive and induce loose enemies.
            else
            {
                leaderHive.destroy();
            }

        }
        else
        {

        }
        enemies.Remove(e);
    }

}
