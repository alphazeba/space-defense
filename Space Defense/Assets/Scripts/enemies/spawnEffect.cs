﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnEffect : MonoBehaviour {

    private Vector3 baseScale;
    public float effectLength;
    private float elapsedTime;


	// Use this for initialization
	void Awake () {
        baseScale = this.transform.localScale;
        this.transform.localScale = new Vector3();
	}
	
	// Update is called once per frame
	void Update () {
        elapsedTime += Time.deltaTime;
        if(elapsedTime > effectLength)
        {
            finish();
        }
        else
        {
            this.transform.localScale = baseScale * (elapsedTime / effectLength);
        }
	}

    private void finish()
    {
        this.transform.localScale = baseScale;
        Destroy(this);//destroy this script once the enemy is done spawing.
    }
}
