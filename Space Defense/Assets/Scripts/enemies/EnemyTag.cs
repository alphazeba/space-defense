﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyTag : MonoBehaviour {

    public float signatureRadius = 100.0f;

    private EnemyTagController controller;
    private EnemyUITag UITag;

    // Use this for initialization
    void Start () {
        controller = FindObjectOfType<EnemyTagController>();

        //when a tag is created (on an enemy) it tells the controller to make its UI component
        UITag = controller.AddTag(this);
    }

    private void OnDestroy()
    {
        //similarly, when a tag is destroyed it tells the controller to delete its UI component
        controller.RemoveTag(UITag);
    }
}
