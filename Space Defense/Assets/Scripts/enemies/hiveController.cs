﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hiveController {

    public float positionFloor = 300;
    public float positionCieling = 600;
    public float spd = 10;

    private Vector3 position;

    private List<GameObject> enemies;
    private GameObject leader;
    private GameObject target;

    private float radius;
    private float radiusGrowthPerMember = 1.0f;
    private float radiusBaseSize = 20;

    private float updateRate = 1;
    private float timer = 0;


    enum modes { wander, pickup, attack };

    modes mode;

    public hiveController(GameObject l)
    {
        leader = l;
        leader.GetComponent<enemyController>().inHive = this;
        position = leader.transform.position;
        enemies = new List<GameObject>();
        setModeWander();

        
        radius = 0;

    }

	

    //TODO remove update. 
    //i want this to be called by the overlord.
	// Update is called once per frame
    //TODO finish this.
	public void manualUpdate () {



        if (leader == null)
        {
            return;
        }


        ////debug
        foreach(GameObject enemy in enemies)
        {
            if(enemy != null)
            {
                Debug.DrawLine(enemy.transform.position, leader.transform.position);
            }
        }

        ///DO hivey things in here.
        ///
        switch (mode)
        {
            case modes.attack:
                attackMode();

                break;

            case modes.pickup:
                pickupMode();
                break;

            case modes.wander:

                wanderMode();
                break;
        }


	}

    #region  private 


    #region ai implementations

    private void attackMode()
    {
        position = leader.transform.position;

        foreach(GameObject enemy in enemies)
        {
            if(enemy != null)
            {

                enemy.GetComponent<enemyController>().setModeChase();
            }
        }

        leader.GetComponent<enemyController>().setModeChase();

        if(leader.GetComponent<enemyController>().getSqrDistFromPlayer() >250 * 250)
        {

        }

    }

    private void pickupMode()
    {
        if(leader != null)
        {
            position = leader.transform.position;
        }
        foreach (GameObject curEnemy in enemies)
        {
            if (curEnemy != null)
            {
                curEnemy.GetComponent<enemyController>().setModeWander(position, radius);

                if (curEnemy.GetComponent<enemyController>().getSqrDistFromPlayer() < 100 * 100)
                {
                    this.setModeAttack();
                }
            }

        }
    }

    private void wanderMode()
    {

        Vector3 potentialPosition = position + Random.onUnitSphere * spd;
        if (withinFlyArea(potentialPosition))
        {
            position = potentialPosition;
        }


        if (enemies.Count > 0)
        {
            foreach (GameObject curEnemy in enemies)
            {
                if(curEnemy != null)
                {
                    curEnemy.GetComponent<enemyController>().setModeWander(position, radius);

                    if (curEnemy.GetComponent<enemyController>().getSqrDistFromPlayer() < 100 * 100)
                    {
                        this.setModeAttack();
                    }
                }
                
            }
        }
            

            
            //radius divided by 10 because leaders should stay close to the middle.
            if(leader != null)
        {

            leader.GetComponent<enemyController>().setModeWander(position, radius / 10);
        }
                
        
        
    }

    #endregion


    #region helper function
    private void updateRadius()
    {
        radius = radiusBaseSize + (enemies.Count * radiusGrowthPerMember);
    }

    private bool withinFlyArea(Vector3 v)
    {

        return v.magnitude > positionFloor && v.magnitude < positionCieling;
       
    }

    #endregion

    #endregion

    #region getters setters

    public bool compareLeader(GameObject leader)
    {
        return this.leader == leader;
    }

    public void addEnemy(GameObject e)
    {
        e.GetComponent<enemyController>().inHive = this;
        enemies.Add(e);
        updateRadius();
    }

    public void removeEnemy(GameObject e)
    {

        if (leader == e)
        {
            leader = null;
        }
        else
        {
            enemies.Remove(e);

            setModeAttack();
        }

        updateRadius();
    }

    public bool isDead()
    {
        return leader == null;
    }

    public bool isPickingUp()
    {
        return mode == modes.pickup;
    }

    public int getSize()
    {
        return enemies.Count;
    }

    public float getSquareLeaderDistance(Vector3 v)
    {
        if (isDead())
        {
            return 1000000;
        }

        Vector3 diff = v - leader.transform.position;
        return diff.sqrMagnitude;
    }


    //merges the other hive into this one. kills the other leader if it is still alive.
    public void merge(hiveController other)
    {
        //removes the other leader from existence.
        if (other.leader != null)
        {
            other.leader.GetComponent<enemyHealth>().health = 0;
        }
        other.leader = null;

        //adds the other list of enemies to this hive.
        this.enemies.AddRange(other.enemies);
        other.enemies.Clear();

        updateRadius();
    }

    public void destroy()
    {
        //TODO give the enemies the command to retreat from the battlefield and unmark the enemies as being  in a hive.
        foreach(GameObject curEnemy in enemies)
        {
            if(curEnemy != null)
            {
                //TODO give retreat command.
                curEnemy.GetComponent<enemyController>().inHive = null;
            }
        }


        //then clear the enemy list and prepare this hive for being killed.
        enemies.Clear();
    }

    #region hive commands

    public void setModeAttack()
    {
        mode = modes.attack;

        foreach (GameObject curEnemy in enemies)
        {
            if(curEnemy != null)
            {

                curEnemy.GetComponent<enemyController>().setModeChase();
            }
        }

        if(leader != null)
        {

            leader.GetComponent<enemyController>().setModeChase();
        }
    }

    public void setModePickup(GameObject target)
    {
        
        this.target = target;
        mode = modes.pickup;

        foreach (GameObject curEnemy in enemies)
        {
            if(curEnemy != null)
            {
                curEnemy.GetComponent<enemyController>().setModeWander(leader.transform.position, radius);
            }
        }

        if(leader != null)
        {
            leader.GetComponent<enemyController>().setModePickup(target);
        }
        else
        {
            mode = modes.wander;
        }
        
        
    }

    public void setModeWander()
    {
        timer = 0;
        wanderMode();
    }

    #endregion

    #endregion
}
