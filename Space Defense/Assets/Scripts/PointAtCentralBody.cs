﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointAtCentralBody : MonoBehaviour {

    private centralBody planet;

    // Use this for initialization
    void Start () {
        planet = FindObjectOfType<centralBody>();
    }
	
	// Update is called once per frame
	void Update () {
        if (planet != null)
        {
            transform.LookAt(planet.transform);
        }
	}
}
