﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AOE : MonoBehaviour {

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.GetComponent<enemyHealth>() != null)
        {
            c.gameObject.GetComponent<enemyHealth>().damage(10);
        }
    }
}
