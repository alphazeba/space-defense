﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//i need to repush this.

public class enemySpawnerController : MonoBehaviour {

    public GameObject enemy;  //the prefab to spawn.
    public int numberToSpawn = 4;  //number of enemies to spawn.
    public float spawnPeriod = 0.8f;  //seconds between spawns.

    #region private vars.

    private float warmUp  = 2; //amount of time prior to first spawn.
    private float coolDown = 1; //amount of time after last spawn before destruction.
    private ParticleSystem emitter;

    private int mode;  //0=warmup  ;  1=spawning  ; 2=cooldown.

    private enemyOverlordController overlord;
    private GameObject player;

    #endregion



    // Use this for initialization
    void Start () {
        mode = 0;
        emitter = GetComponentInChildren<ParticleSystem>();
        warmUp = emitter.duration;
        coolDown = emitter.duration + 0.1f;
        
	}
	
	private void Step()
    {
        switch (mode)
        {
            case 0:
                mode = 1;
                StartCoroutine(stepper(warmUp));
                break;
            case 1:
                
                if(numberToSpawn == 0)
                {
                    mode = 2;
                    emitter.Stop();
                    StartCoroutine(stepper(coolDown));
                }
                else
                {
                    --numberToSpawn;
                    spawn();
                    StartCoroutine(stepper(spawnPeriod));
                }
                break;
            case 2:
                Destroy(gameObject);
                break;
        }
    }

    private IEnumerator stepper(float t)
    {
        yield return new WaitForSeconds(t);
        Step();
    }

    private void spawn()
    {
        GameObject thing = Instantiate(enemy, this.transform.position, this.transform.rotation);
        thing.GetComponent<enemyController>().Initialize(overlord);
        thing = null;
    }

    public void Initialize(GameObject enemy, int numberToSpawn, float spawnPeriod, enemyOverlordController overlord)
    {
        this.enemy = enemy;
        this.numberToSpawn = numberToSpawn;
        this.spawnPeriod = spawnPeriod;
        this.overlord = overlord;

        Step();
    }
    
}
