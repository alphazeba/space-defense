﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidSpawn : MonoBehaviour {

	public GameObject asteroid;
	private float tick = 25.0f;
	private float spawn = 30.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		tick += Time.deltaTime;
		if (tick > spawn) {
			for (int i = 0; i < 20; i++) {
				Vector3 spawn = new Vector3 (Random.Range (-3000.0f, 3000.0f), Random.Range (-1000.0f, 1000.0f), this.transform.position.z);
				Instantiate (asteroid, spawn, Quaternion.identity);
			}
			tick = 0.0f;
		}
	}
}
