﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenController : MonoBehaviour {

	public string sceneToLoad;
	public float delayTime = 3;
	// Use this for initialization
	void Start () {
		StartCoroutine (Delay ());
	}
	
	// Update is called once per frame
	void Update () {

	}

	IEnumerator Delay () {
		yield return new WaitForSeconds (delayTime);
		SceneManager.LoadScene(sceneToLoad);
	}
}


