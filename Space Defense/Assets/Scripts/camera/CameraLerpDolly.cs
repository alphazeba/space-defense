﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLerpDolly : MonoBehaviour {

    public Transform start;
    public Transform end;
    public float amount;
    public float holdOffTime;
    public float destroyAfter = 999.0f;
    public bool atStart;

	// Use this for initialization
	void Start () {
        if (atStart)
        {
            transform.position = start.position;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeSinceLevelLoad > holdOffTime)
        {
            transform.position = Vector3.Lerp(transform.position, end.position, amount);
        }

        if (Time.timeSinceLevelLoad > destroyAfter)
        {
            Destroy(this);
        }
	}
}
