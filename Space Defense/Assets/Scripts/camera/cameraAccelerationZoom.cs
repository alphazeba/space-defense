﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraAccelerationZoom : MonoBehaviour {

    public PlayerControl playerControl;
    public float effectScale = 1;
    public float effectRange = 1000;

    private Vector3 baseScale;

	// Use this for initialization
	void Start () {
        baseScale = this.transform.localScale;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        float value = playerControl.getSpeed()/effectRange; //uses a sigmoid curve.
        value = value / (1 + Mathf.Abs(value));
        this.transform.localScale = baseScale * (1+(value * effectScale));
	}
}
