﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {
    public GameObject target;

    void FixedUpdate () {
        this.transform.rotation = Quaternion.Slerp(transform.rotation, target.transform.rotation, 0.1f);
	}

    void LateUpdate()
    {
        this.transform.position = target.transform.position;
    }
}
