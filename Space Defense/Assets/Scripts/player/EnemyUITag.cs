﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyUITag : MonoBehaviour {

    public EnemyTag myEnemyTag;

    private RawImage im;
    private EnemyTagController controller;

    private bool render = true;

    // Use this for initialization
    void Start () {
        im = GetComponent<RawImage>();
        controller = FindObjectOfType<EnemyTagController>();
    }
	
	// Update is called once per frame
	void Update () {
        Color current = im.color;

        float distance = Vector3.Distance(controller.ship.transform.position, myEnemyTag.transform.position);
        if (distance > controller.SensorRadius + myEnemyTag.signatureRadius) //spheres not touching at all
        {
            render = false;
            im.color = new Color(current.r, current.g, current.b, 0.0f);
        }
        else //the spheres are touching
        {
            render = true;
            if (controller.SensorRadius >= myEnemyTag.signatureRadius) //player is bigger
            {
                im.color = new Color(current.r, current.g, current.b, 1 - (distance - myEnemyTag.signatureRadius) / (controller.SensorRadius - myEnemyTag.signatureRadius));
            }
            else //enemy is bigger
            {
                im.color = new Color(current.r, current.g, current.b, 1 - (distance - controller.SensorRadius) / (myEnemyTag.signatureRadius - controller.SensorRadius));
            }
        }
    }

    void LateUpdate()
    {
        if (render)
        {
            Vector3 screenPos = Camera.main.WorldToScreenPoint(myEnemyTag.transform.position);

            if (screenPos.z > 0
             && screenPos.x > 0 && screenPos.x < Screen.width
             && screenPos.y > 0 && screenPos.y < Screen.height
             ) //on screen
            {
                screenPos.z = 0.0f;
                transform.position = screenPos;
                im.texture = controller.frontTexture;
                transform.localRotation = Quaternion.identity;
            }
            else if (screenPos.z < 0) //behind us
            {
                //fixes weird offset thing
                screenPos.x = Screen.width - screenPos.x;
                screenPos.y = Screen.height - screenPos.y;

                if (screenPos.x > 0 && screenPos.x < Screen.width
                 && screenPos.y > 0 && screenPos.y < Screen.height) //on screen
                {
                    //push it to the edge of the screen somehow
                    screenPos = PushToEdge(screenPos);
                    screenPos = ClampToEdge(screenPos);
                }
                else
                {
                    screenPos = ClampToEdge(screenPos);
                }

                transform.position = screenPos;
                im.texture = controller.arrowTexture;
                Vector3 screenCenter = new Vector3(Screen.width, Screen.height) / 2.0f;
                transform.LookAt(transform.position + Vector3.forward, screenPos - screenCenter);
            }
            else //off screen in front of us
            {
                screenPos = ClampToEdge(screenPos);

                transform.position = screenPos;
                im.texture = controller.arrowTexture;
                Vector3 screenCenter = new Vector3(Screen.width, Screen.height) / 2.0f;
                transform.LookAt(transform.position + Vector3.forward, screenPos - screenCenter);
            }
        }
    }

    private Vector3 ClampToEdge(Vector3 v)
    {
        if (v.x < 0) v.x = 0;
        if (v.x > Screen.width) v.x = Screen.width;

        if (v.y < 0) v.y = 0;
        if (v.y > Screen.height) v.y = Screen.height;

        v.z = 0;

        return v;
    }

    private Vector3 PushToEdge(Vector3 v)
    {
        Vector3 screenCenter = new Vector3(Screen.width, Screen.height) / 2.0f;

        Vector3 delta = v - screenCenter;

        Vector3 screenPos = screenCenter + delta;
        while (screenPos.x > 0 && screenPos.x < Screen.width
            && screenPos.y > 0 && screenPos.y < Screen.height)
        {
            delta *= 1.01f;
            screenPos = screenCenter + delta;
        }

        v = screenCenter + delta;

        v.z = 0;

        return v;
    }
}
