﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileScript : MonoBehaviour {

    private Rigidbody rb;

    private Transform target;
    public TrailRenderer tr;
    public GameObject explosion;

    public float speed = 200;
    private float burstSpeed = 1500;
    private float forwardBurstSpeed = 3000;
    private bool started = false;
    private float inaccuracy = 45f;
    private float delayTime = 0.5f;

    private float lifetime = 10f;

    public void SetTarget(Transform t)
    {
        target = t;
    }

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(start());
        tr.enabled = false;

        Quaternion initial = transform.rotation;
        Quaternion inaccuracyQ = Quaternion.Euler(Random.Range(-inaccuracy, inaccuracy), Random.Range(-inaccuracy, inaccuracy), Random.Range(-inaccuracy, inaccuracy));
        transform.rotation *= inaccuracyQ;
        rb.AddRelativeForce(Vector3.forward * burstSpeed);
        transform.rotation = initial;
        rb.AddRelativeForce(Vector3.forward * forwardBurstSpeed);
    }
	
	void FixedUpdate () {
        if (started)
        {
            if (target == null)
            {
                rb.AddRelativeForce(Vector3.forward * speed);
                lifetime -= Time.deltaTime;
            }
            else
            {
                transform.LookAt(target);

                rb.AddRelativeForce(Vector3.forward * speed);

                transform.LookAt(transform.position + rb.velocity);
            }
        }

        if (lifetime < 0.0f)
        {
            Destroy(this.gameObject);
        }
	}

    IEnumerator start()
    {
        yield return new WaitForSeconds(delayTime);
        started = true;
        tr.enabled = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<enemyHealth>() != null)
        {
            collision.gameObject.GetComponent<enemyHealth>().damage(10);
        }
        GameObject g = Instantiate(explosion, transform.position, transform.rotation);
        tr.gameObject.transform.parent = g.transform;
        Destroy(this.gameObject);
    }
}
