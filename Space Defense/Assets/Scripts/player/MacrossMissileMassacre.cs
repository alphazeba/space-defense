﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MacrossMissileMassacre : MonoBehaviour {

    public GameObject missile;
    public int initialNumVolleys = 3;
    public int volleySize = 100;
    public AudioSource a;

    private int numVolleys;

    // Use this for initialization
    void Start() {
        numVolleys = initialNumVolleys;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonDown(1))
        {
            if (numVolleys > 0)
            {
                numVolleys--;
                a.Play();
                enemyController[] enemies = FindObjectsOfType<enemyController>();
                for (int x = 0; x < volleySize; x++)
                {
                    GameObject g = Instantiate(missile, transform.position + transform.forward * 3, transform.rotation);
                    g.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity;
                    if (enemies.Length > 0)
                    {
                        g.GetComponent<MissileScript>().SetTarget(enemies[x % enemies.Length].transform);
                    }
                }
            }
        }
    }

    public int GetVolleys()
    {
        return numVolleys;
    }

    void Awake()
    {
        Messenger.AddListener(GameEvent.BONUS_VOLLEY, onBonusVolley);
    }

    void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.BONUS_VOLLEY, onBonusVolley);
    }

    void onBonusVolley()
    {
        numVolleys++;
    }
}
