﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGetHumans : MonoBehaviour
{

    private npcSpawnController npcs;
    private Text t;

    // Use this for initialization
    void Start()
    {
        npcs = GameObject.Find("NPCSpawner").GetComponent<npcSpawnController>();
        t = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        t.text = npcs.GetHumans().ToString() + "   ";
    }
}