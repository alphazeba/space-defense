﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour {

    public GameObject crosshairObject;
    private float maxDistance = 600.0f;
    private float aimAssistWidth = 2.0f;
    public RawImage uiCursor;
    public Texture textureNormal;
    public Texture textureLocked;
    private float targetStickyTime = 1.0f;

    void FixedUpdate()
    {
        Ray r = new Ray(transform.position, transform.forward);
        RaycastHit hitInfo;
        if (Physics.SphereCast(r, aimAssistWidth, out hitInfo, maxDistance, 1 << 9)) //hit enemy
        {
            crosshairObject.transform.position = hitInfo.transform.position;
            GetComponent<PlayerShooting>().SetTargetedEnemy(hitInfo.transform.gameObject);
            uiCursor.texture = textureLocked;
        }
        else if (Physics.Raycast(r, out hitInfo, maxDistance)) //hit *anything*
        {
            if (GetComponent<PlayerShooting>().GetTargetedEnemy() == null)
            {
                crosshairObject.transform.position = hitInfo.point;
                uiCursor.texture = textureNormal;
            }
            else
            {
                crosshairObject.transform.position = GetComponent<PlayerShooting>().GetTargetedEnemy().transform.position;
                StartCoroutine(unTarget());
            }
            
        }
        else
        {
            if (GetComponent<PlayerShooting>().GetTargetedEnemy() == null) //hit *nothing*
            {
                crosshairObject.transform.position = transform.position + transform.forward * maxDistance;
                uiCursor.texture = textureNormal;
            }
            else
            {
                crosshairObject.transform.position = GetComponent<PlayerShooting>().GetTargetedEnemy().transform.position;
                StartCoroutine(unTarget());
            }
        }
    }

    IEnumerator unTarget()
    {
        yield return new WaitForSeconds(targetStickyTime);
        StopAllCoroutines();
        GetComponent<PlayerShooting>().SetTargetedEnemy(null);
    }
}
