﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICrosshair : MonoBehaviour {

    public Camera cam;
    public RawImage target;

    public void FixedUpdate()
    {
        Vector3 v = cam.WorldToScreenPoint(transform.position);
        v.z = -1;
        target.rectTransform.position = Vector3.Lerp(target.rectTransform.position, v, 1.0f);
    }
}
