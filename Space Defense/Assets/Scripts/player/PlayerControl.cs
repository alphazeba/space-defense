﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    private float maxSpeed = 100.0f;

    private float rollSpd = 100;
    private float yawSpd = 200;
    private float pitchSpd = 200;

    private float acceleration = 6000;
    private float horizontalAccelerationMultiplier = 0.5f;

    private float dRoll;
    private float dYaw;
    private float dPitch;

    private Vector3 dVelocity = new Vector3();

    private Quaternion goalRotation;

    //the rigidbody attached to this object
    private Rigidbody rb;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();

        goalRotation = this.transform.rotation;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //cap velocity to maxSpeed
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);

        //get pitch and yaw input
        dPitch = -Mathf.Clamp(Input.GetAxis("Mouse Y"), -1.0f, 1.0f) * pitchSpd * Time.deltaTime;
        dYaw = Mathf.Clamp(Input.GetAxis("Mouse X"), -1.0f, 1.0f) * yawSpd * Time.deltaTime;

        //get forward / backward input
        dVelocity.z = Input.GetAxis("Vertical") * acceleration * Time.deltaTime;
        if(dVelocity.z < 0) //can't go backwards as fast.
        {
            dVelocity /= 2;
        }

        //horizontal movement
        dVelocity.x = Input.GetAxis("Horizontal") * acceleration * Time.deltaTime;
        dVelocity.x *= horizontalAccelerationMultiplier;

        //vertical movement
        dVelocity.y = Input.GetAxis("Fly") * acceleration * Time.deltaTime;
        dVelocity.y *= horizontalAccelerationMultiplier;

        //get roll input
        dRoll = -Input.GetAxis("Roll") * rollSpd * Time.deltaTime;

        //apply ship rotation
        goalRotation *= Quaternion.Euler(dPitch, dYaw, dRoll);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, goalRotation, 0.1f);

        //forward thrust
        GetComponent<Rigidbody>().AddRelativeForce(dVelocity);
	}

    public float getSpeed()
    {
        return GetComponent<Rigidbody>().velocity.magnitude;
    }

    public float getMaxSpeed()
    {
        return maxSpeed;
    }
}
