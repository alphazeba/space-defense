﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGetVolleys: MonoBehaviour
{

    public MacrossMissileMassacre mmm;
    private Text t;

    // Use this for initialization
    void Start()
    {
        t = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        t.text = mmm.GetVolleys().ToString();
    }
}
