﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyTagController : MonoBehaviour {

    public GameObject tagPrefab;
    public Transform ship;

    public float minSensorRadius = 100.0f;
    public float maxSensorRadius = 2000.0f;
    private float sensorRadius;

    public Texture frontTexture;
    public Texture arrowTexture;

    private enemyOverlordController eoc;

    public float SensorRadius
    {
        get
        {
            return sensorRadius;
        }
    }

    // Use this for initialization
    void Start () {
        eoc = FindObjectOfType<enemyOverlordController>();
        sensorRadius = minSensorRadius;
	}

	// Update is called once per frame
	void Update () {
        sensorRadius = Mathf.Lerp(sensorRadius, Mathf.Lerp(minSensorRadius, maxSensorRadius, 1 - Mathf.Sqrt(Mathf.Sqrt(eoc.EnemyCount() / 100.0f))), 0.01f);
	}

    //the controller manages UI components for any tags that are created or destroyed
    public EnemyUITag AddTag(EnemyTag t)
    {
        GameObject g = Instantiate(tagPrefab, this.transform);
        EnemyUITag UITag = g.GetComponent<EnemyUITag>();
        UITag.myEnemyTag = t;
        return UITag;
    }

    public void RemoveTag(EnemyUITag t)
    {
        if (t != null) //editor check
        {
            Destroy(t.gameObject);
        }
    }
}
