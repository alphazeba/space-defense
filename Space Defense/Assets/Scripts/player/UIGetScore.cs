﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGetScore : MonoBehaviour {

    private scoreKeeperController sk;
    private Text t;

    private int score = 0;

	// Use this for initialization
	void Start () {
        sk = GameObject.Find("scoreKeeper").GetComponent<scoreKeeperController>();
        t = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        if (sk.getScore() > score)
        {
            score += Mathf.CeilToInt((sk.getScore() - score) / 10.0f);
        }
        
        t.text = score.ToString();
	}
}
