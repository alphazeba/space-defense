﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGetHealth : MonoBehaviour
{

    public PlayerHealth ph;
    private Text t;

    // Use this for initialization
    void Start()
    {
        t = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ph.GetHealth() > 0)
        {
            t.text = ph.GetHealth().ToString() + "%";
        }
        else
        {
            t.text = "DEAD";
        }
    }
}
