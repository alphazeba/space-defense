﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

    private Rigidbody rb;
    public GameObject bulletPrefab;
    public Transform[] bulletSources;
    public GameObject muzzleFlash;

    private float shootCooldown = 0.1f;
    private float shootTime = 0.0f;
    private int firingSource = 0;

    private float inaccuracy = 0.5f;

    private GameObject targetedEnemy;

    public GameObject GetTargetedEnemy()
    {
        return targetedEnemy;
    }

    public void SetTargetedEnemy(GameObject value)
    {
        targetedEnemy = value;
    }

    // Use this for initialization
    void Start () {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetMouseButton(0))
        {
            if (shootTime < 0)
            {
                Quaternion inaccuracyQ = Quaternion.Euler(Random.Range(-inaccuracy, inaccuracy), Random.Range(-inaccuracy, inaccuracy), Random.Range(-inaccuracy, inaccuracy));
                if (targetedEnemy != null)
                {
                    Vector3 interceptPoint = FirstOrderIntercept(bulletSources[firingSource].position + rb.velocity * Time.fixedDeltaTime * 2, Vector3.zero, 500.0f, targetedEnemy.transform.position - rb.velocity * Time.fixedDeltaTime * 2, targetedEnemy.GetComponent<Rigidbody>().velocity);
                    Debug.DrawLine(bulletSources[firingSource].position + rb.velocity * Time.fixedDeltaTime * 2, interceptPoint);

                    Vector3 direction = (interceptPoint - transform.position).normalized;
                    Quaternion firingDirection = Quaternion.LookRotation(direction);
                    if (Quaternion.Angle(Quaternion.LookRotation(transform.forward), firingDirection) < 45.0f)
                    {
                        Instantiate(bulletPrefab, bulletSources[firingSource].position + rb.velocity * Time.fixedDeltaTime * 2, firingDirection * inaccuracyQ);
                        Instantiate(muzzleFlash, bulletSources[firingSource]);
                    }
                    else
                    {
                        Instantiate(bulletPrefab, bulletSources[firingSource].position + rb.velocity * Time.fixedDeltaTime * 2, bulletSources[firingSource].rotation * inaccuracyQ);
                        Instantiate(muzzleFlash, bulletSources[firingSource]);
                    }
                }
                else
                {
                    Instantiate(bulletPrefab, bulletSources[firingSource].position + rb.velocity * Time.fixedDeltaTime * 2, bulletSources[firingSource].rotation * inaccuracyQ);
                    Instantiate(muzzleFlash, bulletSources[firingSource]);
                }
                shootTime = shootCooldown;
                firingSource++;
                firingSource = firingSource % bulletSources.Length;
            }
        }

        //update shooting time.
        shootTime -= Time.deltaTime;
    }

    //first-order intercept using absolute target position
    public static Vector3 FirstOrderIntercept
    (
        Vector3 shooterPosition,
        Vector3 shooterVelocity,
        float shotSpeed,
        Vector3 targetPosition,
        Vector3 targetVelocity
    )
    {
        Vector3 targetRelativePosition = targetPosition - shooterPosition;
        Vector3 targetRelativeVelocity = targetVelocity - shooterVelocity;
        float t = FirstOrderInterceptTime
        (
            shotSpeed,
            targetRelativePosition,
            targetRelativeVelocity
        );
        return targetPosition + t * (targetRelativeVelocity);
    }
    //first-order intercept using relative target position
    public static float FirstOrderInterceptTime
    (
        float shotSpeed,
        Vector3 targetRelativePosition,
        Vector3 targetRelativeVelocity
    )
    {
        float velocitySquared = targetRelativeVelocity.sqrMagnitude;
        if (velocitySquared < 0.001f)
            return 0f;

        float a = velocitySquared - shotSpeed * shotSpeed;

        //handle similar velocities
        if (Mathf.Abs(a) < 0.001f)
        {
            float t = -targetRelativePosition.sqrMagnitude /
            (
                2f * Vector3.Dot
                (
                    targetRelativeVelocity,
                    targetRelativePosition
                )
            );
            return Mathf.Max(t, 0f); //don't shoot back in time
        }

        float b = 2f * Vector3.Dot(targetRelativeVelocity, targetRelativePosition);
        float c = targetRelativePosition.sqrMagnitude;
        float determinant = b * b - 4f * a * c;

        if (determinant > 0f)
        { //determinant > 0; two intercept paths (most common)
            float t1 = (-b + Mathf.Sqrt(determinant)) / (2f * a),
                    t2 = (-b - Mathf.Sqrt(determinant)) / (2f * a);
            if (t1 > 0f)
            {
                if (t2 > 0f)
                    return Mathf.Min(t1, t2); //both are positive
                else
                    return t1; //only t1 is positive
            }
            else
                return Mathf.Max(t2, 0f); //don't shoot back in time
        }
        else if (determinant < 0f) //determinant < 0; no intercept path
            return 0f;
        else //determinant = 0; one intercept path, pretty much never happens
            return Mathf.Max(-b / (2f * a), 0f); //don't shoot back in time
    }
}
