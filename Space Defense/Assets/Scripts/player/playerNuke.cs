﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerNuke : MonoBehaviour {

    public int initialNumNukes = 3;
    public float nukeRadius = 200;
    public int nukeDmg = 100;
    public GameObject explosion;



    private int numNukes;
    private float sqrNukeRadius;

    void Awake()
    {
        Messenger.AddListener(GameEvent.BONUS_NUKE, onBonusNuke);
    }

    void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.BONUS_NUKE, onBonusNuke);
    }

	// Use this for initialization
	void Start () {
        numNukes = initialNumNukes;
        setNukeRadius(nukeRadius);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(1))
        {
            LaunchNuke();
        }
	}

    void LaunchNuke()
    {
        if(numNukes > 0)
        {
            --numNukes;//subtract the nuke


            //deal damage to all enemies within range.
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            for(int i = 0; i < enemies.Length; ++i)
            {
                float sqrDist = (enemies[i].transform.position - this.transform.position).sqrMagnitude;
                if(sqrDist < sqrNukeRadius)
                {
                    enemies[i].GetComponent<enemyHealth>().damage(nukeDmg);
                }
            }

            //create the explosion graphic.
            Instantiate(explosion, this.transform.position, this.transform.rotation);
        }
    }

    public void giveNuke(int n)
    {
        numNukes += n;
    }

    public void setNukeRadius(float r)
    {
        sqrNukeRadius = r * r;
    }

    private void onBonusNuke()
    {
        giveNuke(1);
    }

    public int GetNukes()
    {
        return numNukes;
    }
}
