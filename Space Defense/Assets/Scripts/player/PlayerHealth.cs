﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    public int maxHealth = 10;
    private int health;
    private bool dead = false;

    public Material shieldMaterial;

    public GameObject explode;

    public GameObject gameover;
    public GameObject[] engines;
    public AudioSource[] sounds;

	// Use this for initialization
	void Start () {
        health = maxHealth;
        StartCoroutine(unPulse());
    }

    public void Damage(int damage)
    {
        health -= damage;

        //pulse the shields
        if (!dead)
        {
            shieldMaterial.color = new Color(0, 0, 1.0f, 1.0f);
            //StartCoroutine(unPulse());
        }

        if (health <= 0 && !dead)
        {
            Instantiate(explode, this.transform.position, this.transform.rotation);
            GetComponent<PlayerControl>().enabled = false;
            GetComponent<PlayerShooting>().enabled = false;
            GetComponent<MacrossMissileMassacre>().enabled = false;
            GetComponent<Crosshair>().enabled = false;
            GetComponent<Rigidbody>().drag = 0.0f;
            GetComponent<Rigidbody>().freezeRotation = false;
            GetComponent<Rigidbody>().AddTorque(1, 1, 1, ForceMode.Impulse);
            gameover.SetActive(true);
            foreach(GameObject g in engines)
            {
                g.SetActive(false);
            }
            foreach (AudioSource a in sounds)
            {
                a.volume = 0.0f;
            }


            dead = true;
            StartCoroutine(unPulse());
        }
    }

    public void Heal(int heal)
    {
        health += heal;
    }

    public void HealToFull()
    {
        health = maxHealth;
    }

    public int GetHealth()
    {
        return health;
    }

    IEnumerator unPulse()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            if (shieldMaterial.color.a > 0.0f)
            {
                shieldMaterial.color = new Color(0, 0, 1.0f, shieldMaterial.color.a - 0.1f);
            }
            //StartCoroutine(unPulse());
        }
    }

    private void OnDestroy()
    {
        shieldMaterial.color = new Color(0, 0, 1.0f, 0);
    }
}
