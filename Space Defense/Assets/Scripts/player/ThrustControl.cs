﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrustControl : MonoBehaviour {

    public float maxSpeed = 3.0f;

    private ParticleSystem.MainModule m;

    private Light l;

	// Use this for initialization
	void Start () {
        m = GetComponent<ParticleSystem>().main;
        l = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        m.startSpeed = Mathf.Lerp(0, maxSpeed, Input.GetAxis("Vertical"));
        l.intensity = Mathf.Lerp(0, 8, Input.GetAxis("Vertical"));
	}
}
