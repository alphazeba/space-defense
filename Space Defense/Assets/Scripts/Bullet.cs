﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed = 10.0f;
    public float lifeTime = 2.0f;
    public GameObject explodeFX;

    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        rb.velocity += transform.forward * speed;
    }

    void Update()
    {
        lifeTime -= Time.deltaTime;

        if (lifeTime < 0)
        {
            die();
        }

        Ray r = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, rb.velocity.magnitude * Time.fixedDeltaTime))
        {
            if (!hit.transform.gameObject.CompareTag("Boss"))
            {
                if (!hit.transform.gameObject.CompareTag("Player"))
                {
                    Instantiate(explodeFX, hit.point, transform.rotation);
                    //die();
                    rb.velocity = Vector3.zero;

                    if (hit.transform.gameObject.CompareTag("Enemy"))
                    {
                        hit.transform.gameObject.GetComponent<enemyHealth>().damage(1);
                    }
                }
            }
            
        }
    }

    private void die()
    {
        Destroy(gameObject);
    }
}
