﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateAfterTime : MonoBehaviour {

    public GameObject obj;
    public float time;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeSinceLevelLoad > time)
        {
            Instantiate(obj, this.transform);
            Destroy(this);
        }
	}
}
