﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFOVChange : MonoBehaviour {

    public float startTime;
    public float goalFOV;

    private Camera cam;

    public float speed;

	// Use this for initialization
	void Start () {
        cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeSinceLevelLoad > startTime)
        {
            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, goalFOV, speed);
        }
	}
}
