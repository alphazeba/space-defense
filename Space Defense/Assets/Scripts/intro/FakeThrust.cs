﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeThrust : MonoBehaviour {

    private ParticleSystem.MainModule m;
    private Light l;
    public float maxSpeed = 3.0f;

    private Vector3 last;

    // Use this for initialization
    void Start () {
        m = GetComponent<ParticleSystem>().main;
        l = GetComponent<Light>();
        last = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        m.startSpeed = Mathf.Lerp(0, maxSpeed, Vector3.Distance(last, transform.position));
        l.intensity = Mathf.Lerp(0, 8, Vector3.Distance(last, transform.position));

        last = transform.position;
    }
}
