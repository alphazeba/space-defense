﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class npcSpawnController : MonoBehaviour {

	public GameObject npc;
	public int numToSpawn;

    private centralBody[] planets;

    private List<GameObject> humans = new List<GameObject>();
	// Use this for initialization
	void Start () {
        planets = FindObjectsOfType<centralBody>();

        for (int i = 0; i < numToSpawn; i++) {
				humans.Add(Instantiate(npc, Random.onUnitSphere * planets[i % planets.Length].getRadius() + planets[i % planets.Length].transform.position, this.transform.rotation));
		}
	}
	
	// Update is called once per frame
	void Update () {
        for (var i = humans.Count - 1; i > -1; i--)
        {
            if (humans[i] == null)
                humans.RemoveAt(i);
        }
    }

    public int GetHumans()
    {
        return humans.Count;
    }
}
