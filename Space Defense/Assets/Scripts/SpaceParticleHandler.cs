﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceParticleHandler : MonoBehaviour {

    public GameObject spaceParticle;
    private int count = 200;
    private float radius = 75.0f;

    private List<GameObject> particles = new List<GameObject>();

	// Use this for initialization
	void Start () {
        for (int x = 0; x < count; x++)
        {
            GameObject g = Instantiate(spaceParticle, transform.position + Random.insideUnitSphere * radius, transform.rotation);
            g.GetComponent<SpaceParticleScript>().SetPlayer(this.gameObject);
            particles.Add(g);
        }
	}
	
	void FixedUpdate () {
		foreach(GameObject g in particles)
        {
            if (distance(g.transform.position, this.transform.position) > radius)
            {
                g.transform.position += -(g.transform.position - this.transform.position) * 2.0f;
            }
        }
	}

    float distance(Vector3 one, Vector3 two)
    {
        return (one - two).magnitude;
    }
}
