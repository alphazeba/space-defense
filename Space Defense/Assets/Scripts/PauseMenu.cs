﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public Canvas c;

    public Button resumeButton;
    public Button quitButton;

	// Use this for initialization
	void Start () {
        resumeButton.onClick.AddListener(onResume);
        quitButton.onClick.AddListener(onQuitButton);
        c.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0.0f;
            c.enabled = true;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
	}

    void onResume()
    {
        Time.timeScale = 1.0f;
        c.enabled = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void onQuitButton()
    {
        SceneManager.LoadScene("menuScene");
        Time.timeScale = 1.0f;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
