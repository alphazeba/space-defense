﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scoreKeeperController : MonoBehaviour {


    public int extraVolleyPoints = 10000;

    private int wave;
    private int score;

    private int nextVolley;


    void Awake()
    {
        Messenger.AddListener(GameEvent.ENEMY_HIT, enemyHit);
        Messenger.AddListener(GameEvent.ENEMY_DIED, enemyDied);
        Messenger.AddListener(GameEvent.END_OF_WAVE, endOfWave);

        nextVolley = extraVolleyPoints;
    }

    void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.ENEMY_HIT, enemyHit);
        Messenger.RemoveListener(GameEvent.ENEMY_DIED, enemyDied);
        Messenger.RemoveListener(GameEvent.END_OF_WAVE, endOfWave);
    }

	// Use this for initialization
	void Start () {
        score = 0;
        wave = 1;
	}

    void endOfWave()
    {
        int survivors = GameObject.FindGameObjectsWithTag("Person").Length;
        
        addScore(250 * survivors * wave);
        wave++;
    }

    void enemyHit()
    {
        addScore(10);
    }

    void enemyDied()
    {
        addScore(50);
    }

    public int getScore()
    {
        return score;
    }

    private void checkForBonusVolley()
    {
        if(score > nextVolley)
        {
            Messenger.Broadcast(GameEvent.BONUS_VOLLEY);
            nextVolley += extraVolleyPoints;
        }
    }

    private void addScore(int n)
    {
        score += n;
        checkForBonusVolley();
    }
}
