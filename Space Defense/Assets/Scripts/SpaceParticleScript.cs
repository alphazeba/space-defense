﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceParticleScript : MonoBehaviour {

    private GameObject player;
    public Material mat;
    public float max;

    private Rigidbody playerRB;

    public void SetPlayer(GameObject value)
    {
        player = value;
    }

    // Use this for initialization
    void Start () {
        playerRB = player.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

        float factor = Mathf.Lerp(0, 1, playerRB.velocity.magnitude / player.GetComponent<PlayerControl>().getMaxSpeed());

        if (playerRB.velocity.magnitude != 0)
        {
            transform.rotation = Quaternion.LookRotation(playerRB.velocity, player.transform.up);
        }
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, factor);
        mat.color = new Color(1.0f, 1.0f, 1.0f, Mathf.Sqrt(factor));
	}

    private void OnDestroy()
    {
        mat.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    }
}
