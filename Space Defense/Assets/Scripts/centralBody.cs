﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class centralBody : MonoBehaviour {

    public float radius = 200;

    private Vector3 pos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public float getRadius()
    {
        return radius;
    }

    public Vector3 getPos()
    {
        return this.transform.position;
    }
}
