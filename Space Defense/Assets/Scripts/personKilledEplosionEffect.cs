﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class personKilledEplosionEffect : MonoBehaviour {
    public float maxScale  = 1000;
    public float effectTime = 3;

    private float timer;
	// Use this for initialization
	void Start () {
        timer = 0;
        this.transform.localScale = new Vector3();
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        float size = timer / effectTime * maxScale;
        this.transform.localScale = new Vector3(size, size, size);

        if (timer > effectTime)
        {
            Destroy(gameObject);
        }
	}
}
