﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyLanderRotationAnimation : MonoBehaviour {

    public float rotSpd;

    private float rot;

	// Use this for initialization
	void Start () {
        rot = 0;
	}
	
	// Update is called once per frame
	void Update () {
        rot += rotSpd * Time.deltaTime;
        this.transform.localRotation = Quaternion.Euler(rot, 0, -90);
	}
}
