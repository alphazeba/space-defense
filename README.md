This is a project developed by myself and Warren Casey.


This is a unity project and was worked on using the built in source control.
After primary development I pushed it here as the unity source control seemed finicky.

There is a lot that needs to be added to this to be "complete" however it is unlikely to be completed
due to the number of poorly designed items.

This project to have succeeded required a longer development period, as we were only given a couple weeks.
Additionally, a more complete plan should have designed from the outset.  Many features were tacked on 
preexisting objects that were not designed to accomodate such features, which was a tragic oversight since the project 
is so small to begin with.  I think it would be fun to recreate this project having a complete picture and plan.
This would likely result in drastically reduced dev time, vastly increased optimization, and significantly
more "intelligent" enemies.